import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  async getStatus(): Promise<object> {
    return {
      result: true,
      response: 'OK',
    };
  }
}
