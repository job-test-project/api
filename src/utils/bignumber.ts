import { BigNumber } from 'bignumber.js';

export type TBNArg = BigNumber | string | number;

export const sum = (...args: TBNArg[]): BigNumber =>
  // @ts-expect-error typescript incorrectly judges reduce interface
  args.reduce((a, b) => a.plus(b), new BigNumber(0));

export const div = (...args: TBNArg[]): BigNumber =>
  // @ts-expect-error typescript incorrectly judges reduce interface
  args.reduce((a, b) => {
    return new BigNumber(a).dividedBy(b);
  });
export const pow = (...args: TBNArg[]): BigNumber =>
  // @ts-expect-error typescript incorrectly judges reduce interface
  args.reduce((a, b) => {
    return new BigNumber(a).pow(b);
  });

export const gt = (a: TBNArg, b: TBNArg): boolean =>
  new BigNumber(a).isGreaterThan(b);
