import * as assert from 'node:assert';

export const apiConfig = {
  port: Number(process.env.PORT),
} as const;

assert(
  apiConfig.port && !Number.isNaN(apiConfig.port),
  'process.env.PORT is required',
);
