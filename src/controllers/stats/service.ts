import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';

import { BlocksEntity } from '@project/project-models';

import { blocksRepository, txsRepository } from '../../models';
import { div, gt, pow, sum } from '../../utils/bignumber';

interface IStatsSum {
  [address: string]: string;
}

@Injectable()
export class StatsService {
  private readonly logger = new Logger(StatsService.name);

  private async sumBlockTxs(block: BlocksEntity): Promise<IStatsSum> {
    const txs = await txsRepository.findAll({ blockId: block.id });

    const result: IStatsSum = {};

    for (const tx of txs) {
      if (result[tx.from]) {
        result[tx.from] = sum(
          result[tx.from],
          parseInt(tx.value, 16),
        ).toString();
      } else {
        result[tx.from] = sum(parseInt(tx.value, 16)).toString();
      }

      if (result[tx.to]) {
        result[tx.to] = sum(result[tx.to], parseInt(tx.value, 16)).toString();
      } else {
        result[tx.to] = sum(parseInt(tx.value, 16)).toString();
      }
    }

    return result;
  }

  async calcMostChangedAddress(limit: number): Promise<object> {
    try {
      this.logger.debug(`Starting summarizing`);
      const start = Date.now();

      const blocks = await blocksRepository.findAllLatest({
        limit: limit,
      });
      const promises = blocks.map((b) => this.sumBlockTxs(b));

      const blocksSummary = await Promise.all(promises);

      const addressWithMaxValue: {
        address: string;
        value: string;
      } = {
        address: null,
        value: '0',
      };
      const result: IStatsSum = {};

      for (const summary of blocksSummary) {
        for (const address in summary) {
          if (result[address]) {
            result[address] = sum(result[address], summary[address]).toString();
          } else {
            result[address] = summary[address];
          }

          if (gt(result[address], addressWithMaxValue.value)) {
            addressWithMaxValue.address = address;
            addressWithMaxValue.value = result[address];
          }
        }
      }

      addressWithMaxValue.value = div(
        addressWithMaxValue.value,
        pow(10, 18),
      ).toString();

      this.logger.debug(`Done in ${Date.now() - start}ms`);

      return addressWithMaxValue;
    } catch (e) {
      this.logger.error(`Failed to calculate last block - ${e.message}`);
      throw new HttpException(
        'Internal Server Error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
