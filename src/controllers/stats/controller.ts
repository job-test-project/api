import { Controller, Get, Query } from '@nestjs/common';

import { constants } from '../../configs/constants';
import { StatsGetParams } from './params';

import { StatsService } from './service';

@Controller('stats')
export class StatsController {
  constructor(private statsService: StatsService) {}

  @Get()
  async findAll(@Query() params: StatsGetParams): Promise<object> {
    const limit = params.limit ?? constants.statsDefaultLimit;

    return {
      result: true,
      response: await this.statsService.calcMostChangedAddress(limit),
    };
  }
}
