import { IsOptional, IsNumber } from 'class-validator';

export class StatsGetParams {
  @IsOptional()
  @IsNumber()
  limit?: number;
}
