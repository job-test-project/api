import 'dotenv/config';

import './models';

import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { apiConfig } from './configs/api';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  );

  await app.listen(apiConfig.port);
  Logger.log(`Api is running on port ${apiConfig.port}`);
}

bootstrap();
