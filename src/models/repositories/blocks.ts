import { BlocksEntity } from '@project/project-models';

export class BlocksRepository {
  async findAllLatest(params: { limit: number }): Promise<BlocksEntity[]> {
    // request is faster block by block then join, limit/offset on transactions or indexing not pk fields
    return await BlocksEntity.findAll({
      limit: params.limit,
      order: [['id', 'DESC']],
    });
  }
}
