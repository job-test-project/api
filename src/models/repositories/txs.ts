import { TxsEntity } from '@project/project-models';

export class TxsRepository {
  async findAll(where: { blockId: string }): Promise<TxsEntity[]> {
    return await TxsEntity.findAll({
      where: {
        block_id: where.blockId,
      },
      raw: true,
    });
  }
}
