import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { StatsController } from './controllers/stats/controller';
import { StatsService } from './controllers/stats/service';

@Module({
  imports: [],
  controllers: [AppController, StatsController],
  providers: [StatsService],
})
export class AppModule {}
